function CanvasObject() { }

CanvasObject.elementSize = 1;
CanvasObject.canvas = document.getElementById("c");

//zoom game 
CanvasObject.canvas.width = 320;
CanvasObject.canvas.width = 240;
CanvasObject.canvas.style.width = '1600px';
CanvasObject.canvas.style.width = '1200px';

CanvasObject.context = CanvasObject.canvas.getContext("2d");
CanvasObject.boardSize = 150;

CanvasObject.prototype = {
	checkElementExistence: function (x, y) {
		var data = CanvasObject.context.getImageData(x, y, CanvasObject.elementSize, CanvasObject.elementSize).data;
		for (var i = 0; i < data.length; i++) {
			var element = data[i];
			if (element !== 0) {
				return true;
			}
		}
		return false;
	},
	paint: function (color, position) {
		var prevColor = "rgb(254,0,0)";
		CanvasObject.context.fillStyle = color;
		CanvasObject.context.fillRect(position.x, position.y, CanvasObject.elementSize, CanvasObject.elementSize);
		CanvasObject.context.fillStyle = prevColor;
	},
	clear: function (position) {
		CanvasObject.context.clearRect(position.x, position.y, CanvasObject.elementSize, CanvasObject.elementSize);
	},
	clearBoard: function () {
		CanvasObject.context.clearRect(0, 0, CanvasObject.boardSize, CanvasObject.boardSize);
	},
	createBorder: function (color) {
		CanvasObject.context.fillStyle = color;
		CanvasObject.context.strokeRect(0, 0, CanvasObject.boardSize, CanvasObject.boardSize);
	},
	generateRandomPlace: function () {
		return Math.floor((Math.random() * CanvasObject.boardSize));
	}
};