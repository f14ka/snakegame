"use strict";

var foodInstance = (function () {
    var instance;

    function createInstance() {
        return new Food();
    }

	function Food() {
		this.prototype = CanvasObject.prototype;

		var color = "rgb(0,254,0)";
		var foodPosition = new ObjectPosition(50, 50);
		this.getPosition = function () {
			return { x: foodPosition.x, y: foodPosition.y };
		};

		this.generateFood = function () {
			var putInEmpty = false;
		
			//find empty place to put food there
			while (!putInEmpty) {

				var x = this.prototype.generateRandomPlace();
				var y = this.prototype.generateRandomPlace();

				//check if at this point there is something (check collor difference)
				putInEmpty = !this.prototype.checkElementExistence(x, y);
			}
		
			//put food on empty place
			this.prototype.paint(color, new ObjectPosition(x, y));

			foodPosition.x = x;
			foodPosition.y = y;
		};
	}

    return (function () {
		if (!instance) {
			instance = createInstance();
		}
		return instance;
	})();
})();