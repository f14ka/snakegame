"use strict";

var snakeInstance = (function () {
    var instance;

    function createInstance() {
        return new Snake();
    }

	Snake.directions = { up: "Up", down: "Down", left: "Left", right: "Right" };
	function Snake() {
		this.prototype = CanvasObject.prototype;

		this.hit = new Observer();
		this.snakeLength = new Observer();

		var self = this;
		var color = "rgb(254,0,0)";
		var startPosition;
		var path;

		var currentDirection = Snake.directions.down;
		var food = foodInstance;

		this.reset = function () {
			startPosition = new ObjectPosition(50, 50);
			path = [];
			currentDirection = Snake.directions.down;

			this.prototype.paint(color, startPosition);
			food.generateFood();
		};

		this.moove = function (direction) {
			var pos = { x: startPosition.x, y: startPosition.y };
			path.push(pos);
			currentDirection = direction;

			switch (direction) {
				case Snake.directions.up:
					mooveTo(0, -1);
					break;
				case Snake.directions.down:
					mooveTo(0, 1);
					break;
				case Snake.directions.left:
					mooveTo(-1, 0);
					break;
				case Snake.directions.right:
					mooveTo(1, 0);
					break;
			}
		};

		this.validateMoove = function (direction) {
			if (direction === Snake.directions.up || direction === Snake.directions.down) {
				if (currentDirection === Snake.directions.left || currentDirection === Snake.directions.right) {
					return true;
				}
				return false;
			}
			else if (direction === Snake.directions.left || direction === Snake.directions.right) {
				if (currentDirection === Snake.directions.up || currentDirection === Snake.directions.down) {
					return true;
				}
				return false;
			}
			return false;
		};

		function mooveTo(a, b) {
			startPosition.x += a;
			startPosition.y += b;
			oneStep(currentDirection);
		};

		function oneStep(direction) {
			checkHit();
			self.prototype.paint(color, startPosition);
	
			//when eats - gets bigger - the tail is not cleaned on this iteration
			if (startPosition.x === food.getPosition().x && startPosition.y === food.getPosition().y) {
				food.generateFood();
				self.snakeLength.fire(path.length);
			}
			else if (path.length < 10) {
				self.snakeLength.fire(path.length);
			}
			else {
				var c = path.shift();
				if (c != undefined) {
					self.prototype.clear(c);
				}
			}
		};

		function checkHit() {
			if (startPosition.x === food.getPosition().x && startPosition.y === food.getPosition().y) {
				return false;
			}
			if (self.prototype.checkElementExistence(startPosition.x, startPosition.y)) {
				self.hit.fire();
				return true;
			}
		};
	}

    return (function () {
		if (!instance) {
			instance = createInstance();
		}
		return instance;
	})();
})();