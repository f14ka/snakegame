function Observer() {
    this.handlers = [];  // observers
}

Observer.prototype = {
    subscribe: function (fn) {
        this.handlers.push(fn);
    },

    fire: function (o, thisObj) {
        var scope = thisObj || window;
        this.handlers.forEach(function (item) {
            item.call(scope, o);
        });
    }
}