"use strict";

var gameInstance = (function () {
    var instance;

    function createInstance() {
        var object = new Game();
        return object;
    }

	function Game() {
		var self = this;
		this.prototype = CanvasObject.prototype;

		var speed;
		var borderColor = "rgb(0,0,254)";

		var snake = snakeInstance;
		snake.hit.subscribe(resetGame);
		snake.snakeLength.subscribe(accelate);

		//start game
		resetGame();

		function resetGame() {
			speed = 50;
			self.prototype.clearBoard();
			self.prototype.createBorder(borderColor);
			snake.reset();
			clearInterval(snakeMotion);
		}

		var snakeMotion;
		document.onkeydown = function (e) {
			if (snake.validateMoove(e.keyIdentifier)) {
				clearInterval(snakeMotion);
				snakeMotion = setInterval(function () {
					snake.moove(e.keyIdentifier);
				}, speed);
			}
		};

		function accelate() {
			speed-=2;
		}
	}

    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();

var g = gameInstance.getInstance();