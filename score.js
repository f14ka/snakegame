"use strict";

(function () {
	var score = document.getElementById("hScore");

	var snake = snakeInstance;
	snake.snakeLength.subscribe(setHighScore);

	function setHighScore(snakeLength) {
		if (score.innerHTML < snakeLength) {
			score.innerHTML = snakeLength;
		}
	}
})();